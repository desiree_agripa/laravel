<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\Post;
use App\Models\Profile;
use App\Models\User;
use App\Models\Likes;
use Illuminate\Support\Facades\Gate;

class PostsController extends Controller
{
    public function __construct(){ //cannot access when not login
        $this->middleware('auth');
    }
    /**
    * Fetching all post, user followed and followers created by the auth user
    */
    public function index(POST $post){

       $users = auth()->user()->following()->pluck('profiles.user_id');

        $usersFollowing = auth()->user()->following()->take(5)->get(); //getting all following
        $usersFollowers = auth()->user()->profile->followers()->take(5)->get(); //getting all follower
        
        $usersList = User::whereNotIn('id', $users) // exclude already followed
       ->where('id', '<>', auth()->user()->id)->take(5) // and the user himself
        ->get();

       
        $following = auth()->user()->following()->pluck('profiles.user_id')->toArray();
        $following[] =auth()->user()->id;
        $posts = Post::whereIn('user_id',$following)->with('user','comments')->latest()->paginate(5);
    
        $sharer= auth()->user()->id;
        $sharedPost = Post::where('user_id', $sharer)->whereNotNull('post_shared_id')->get();
        
        $getAllPosts = Post::all();

        

        return view('posts.index', compact('posts','usersFollowing','usersList','usersFollowers','sharedPost','getAllPosts'));
    }


    /**
    * Store post created by the auth user
    */
    public function store(Request $request){
       
            $validator = Validator::make($request->all(),[
                'description' => 'required|max:140',
                'image' => 'image |mimes:jpg,png,jpeg| max:2048',
            ]);

            if($validator->fails())
            {
                return response()->json([
                    'status'=>400,
                    'errors'=>$validator->messages(),
                ]);
            }else{
                   
                    $newPost = new Post();
                    if($request->hasFile('image'))
                    {
                        $imagePath = request('image')->store('uploads','public');
                        $image = Image::make(public_path("storage/$imagePath"))->resize(500, 250);
                        $image->save();
                    }
                    else
                    {
                        $imagePath = '';
                    }

                    auth()->user()->posts()->create([
                        'description' => strip_tags($request->description),
                        'image' => $imagePath,
                    ]);
                
                   return response()->json([
                        'status'=>200,
                        'message'=>'You have successfully created a post.',
                    ]);
            }
           
           
    }

    /**
    * Updating post created by the auth user
    * Validate if post is created by the auth user
    */
    public function update(Request $request, Post $post){

        if(auth()->user()->can('update', $post)){

             $validator = Validator::make($request->all(),[
                 'description' => 'required|max:140',
                 'image' => 'image |mimes:jpg,png,jpeg| max:2048',
             ]);
 
             if($validator->fails()){
                return response()->json([
                    'status'=> 400,
                    'errors'=> $validator->messages(),
                ]);
             }else{

                if($request->hasFile('image')){
                    $photoPath = $request->image->store('uploads','public');
        
                    $photo = Image::make(public_path("storage/$photoPath"))->resize(500, 450);
                    $photo->save();
                   
                    }else{
                        $photoPath = $post->image; 
                    }
                    
                    $post->update([
                        'description' => strip_tags($request->description),
                        'image' => $photoPath,
                    ]);

                    return response()->json([
                        'status'=>200,
                        'message'=>"Post successfully updated!",
                    ]);
             }
         }else{
             return back()->with('warning','Unauthorize to edit this post!'); 
        } 


     }
    
    /**
    * Redirect to another page where the user wants to view specific post 
    * Post with image can redirect in another page
    */
    public function show(Post $post){
        return view('posts.show', compact('post')); 
    }

    /**
    * Deleting post created by the auth user
    * Validate if post is created by the auth user
    */
    public function destroy(Post $post){
        if(auth()->user()->can('delete', $post)){
            $post->delete();
            return back()->with('success','Post successfully deleted!'); 
            
        }else{
            return back()->with('warning','Unauthorize to delete this post!'); 
        }
    }

    /**
    * Sharing of post by the auth user
    * shared post can other followed user post and own post
    */
    public function share(Request $request){

        $share = new Post;
        $share->insert([
            'description' => strip_tags($request->description),
            'user_id' => auth()->user()->id,
            'post_shared_id' => $request->post_id
        ]);

       return back()->with('success','Post shared successfully!');
    }

    

    

}
