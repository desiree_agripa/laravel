<?php

namespace App\Http\Controllers;
use App\Models\Shares;
use Illuminate\Http\Request;

class ShareController extends Controller
{
    public function share(Request $request){

        $share = new Shares;
        $share->insert([
            'description' => strip_tags($request->description),
            'user_id' => $request->user_id,
            'post_id' => $request->post_id
        ]);

       return back()->with('toast_success','Post shared successfully!');
    }
}
