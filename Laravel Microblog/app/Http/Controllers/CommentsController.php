<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\comments;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;


class CommentsController extends Controller
{   
    /**
    * Store comments created by the user
    * Validate comment which cannot be empty and max of 140char
    */
    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'comment' => 'required|string|max:140'
        ]);
        if($validator->fails())
        {
            return back()->with('toast_warning','Error!'); 
        }else{

            $comment = new comments;
            $comment->comment = strip_tags($request->input('comment'));
            $comment->post_id = $request->input('post_id');
            $comment->user_id = auth()->user()->id;
            $comment->save();
            return back()->with('success','Comment successfully created!'); 
        }
    }

    /**
    * Deleting comments created by the auth user
    * Validate if comment is created by the auth user
    */
    public function destroy(comments $comment){

        if(auth()->user()->can('delete', $comment)){
            $comment->delete();
            return back()->with('success','Comment Deleted successfully!'); 
         }else{
            return back()->with('warning','Unauthorize to delete this comment!'); 
         }
             
    }

     /**
    * Update comment created by the auth user
    * Validate if comment is created by the auth user
    */
    public function update(Request $request, comments $comment){
     
        if(auth()->user()->can('update', $comment)){
            $validator = Validator::make($request->all(),[
                'comment' => 'required|max:140',
            ]);

            if($validator->fails()){
                return response()->json([
                    'status'=>400,
                    'errors'=>$validator->messages(),
                ]);
             }else{
                    $comment->update([
                        'comment' => strip_tags($request->comment),
                        ]);
                    return response()->json([
                        'status'=>200,
                        'message'=>"Comment successfully updated!",
                    ]);
             }
        }else{
            return back()->with('warning','Unauthorize to edit this comment!'); 
        }
    }
}
