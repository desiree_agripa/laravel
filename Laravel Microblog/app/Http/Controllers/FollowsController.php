<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\User;

class FollowsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
    * Follow/unfollow a user
    * 
    */
    public function store(User $user){
        
        return auth()->user()->following()->toggle($user->profile);  
    }

   
 
}
