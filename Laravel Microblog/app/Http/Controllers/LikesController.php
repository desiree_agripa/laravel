<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Likes;

class LikesController extends Controller
{

    public function __construct(){ //cannot access when not login
        $this->middleware('auth');
    }

    /**
    * Liking a post 
    * 
    */
    public function store(Post $post){
        
        return auth()->user()->liking()->toggle($post->id);  
        
    }   
   

}
