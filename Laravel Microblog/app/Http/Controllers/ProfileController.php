<?php

namespace App\Http\Controllers;
use \App\Models\User;
use \App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;


class ProfileController extends Controller
{
    /**
    * Fetching all auth user post, follower,following created by the auth user
    */
    public function index(User $user)
    { 

        $postCount = Cache::remember(
            'count.posts.' . $user->id,
            now()->addSeconds(15),
            function() use ($user){
                return $user->posts->count();
        });
        $followerCount = Cache::remember(
            'count.followers.' . $user->id,
            now()->addSeconds(15),
            function() use ($user){
                return $user->profile->followers->count();
        });
        $followingCount = Cache::remember(
            'count.following.' . $user->id,
            now()->addSeconds(15),
            function() use ($user){
                return $user->following->count();
        });

       
        $posts = Post::where('user_id', $user->id)->with('user','comments')->latest()->paginate(5);

        $getAllPosts = Post::all();

        $usersFollowing = $user->following()->take(5)->get(); //getting all following
        $usersFollowers = $user->profile->followers()->take(5)->get(); //getting all follower

        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id): false;

        return view('profiles.index', compact('user', 'follows','postCount','followerCount','followingCount','usersFollowing','usersFollowers','posts','getAllPosts'));
    }

    public function getProfilePost(Request $request){
        $posts = Post::where('user_id', auth()->user()->id)->with('user','comments')->latest()->paginate(5);
        $getAllPosts = Post::all();
        return view('profiles.postdisplay', compact('posts','getAllPosts'));
  
    }

    public function edit(User $user){ 
       $this->authorize('update', $user->profile);
       return view('profiles.edit', compact('user')); 
    
    }
    
    public function update(Request $request, $user){
       
        $validator = Validator::make($request->all(),[
            'firstname' => 'required|string|max:255',
            'middlename'=> 'nullable|string|max:255',
            'lastname' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users,username,'.$user,
            'photo' => 'image|mimes:jpg,png,jpeg| max:2048'
        ]);

        if($validator->fails()){
           return response()->json([
               'status'=>400,
               'errors'=>$validator->messages(),
           ]);
        }else{ 
            $user = User::find($user);
            $user->firstname = strip_tags($request->firstname);
            $user->middlename = strip_tags($request->middlename);
            $user->lastname = strip_tags($request->lastname);
            $user->username = strip_tags($request->username);
            
            if($request->hasFile('photo')){
                $photoPath = request('photo')->store('profile','public');
    
                $photo = Image::make(public_path("storage/$photoPath"))->resize(500, 450);
                $photo->save();
            }else{
                $photoPath = $user->photo; 
            }
            $user->photo = $photoPath;
            $user ->save();

            return response()->json([
                'status'=>200,
                'message'=>"Profile successfully updated!",
            ]);
        }

    }
      public function showFollower(User $user){
        
        $listFollowers = $user->profile->followers()->get(); //getting all follower
        $listFollowings = $user->following()->get(); //getting all following

         return view('profiles.followers', compact('listFollowers','listFollowings'));
     }

     public function showFollowing(User $user){

        $listFollowings = $user->following()->get(); //getting all following
        $listFollowers = $user->profile->followers()->get();
 
         return view('profiles.following', compact('listFollowings','listFollowers'));
     }

     public function showSearch(){

        $searchText = $_GET['query'];

        $searchedPeople = User::where('firstname','LIKE','%'.$searchText.'%')
        ->orWhere('middlename','LIKE','%'.$searchText.'%')
        ->orWhere('lastname','LIKE','%'.$searchText.'%')
        ->get();

        $searchedPost = Post::where('description','LIKE','%'.$searchText.'%') 
        ->get();

        $getAllPosts = Post::all();

        return view('profiles.search', compact('searchedPeople', 'searchedPost','getAllPosts'));
     }

     public function usersFollow(){
    
        $users = auth()->user()->following()->pluck('profiles.user_id');
        $usersFollow = User::whereNotIn('id', $users) // exclude already followed
        ->where('id', '<>', auth()->user()->id) // and the user himself
         ->get();
        
         
         return view('profiles.follow', compact('usersFollow'));
     }
} 
