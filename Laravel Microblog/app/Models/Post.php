<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    protected $dates = ['delete_at'];

    public function likedBy(User $user){
        return $this->likes->contains('user_id', $user->id);
    }

    public function ownedBy(User $user){
        return $user->id === $this->user_id;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function comments(){
        return $this->hasMany(comments::class);
    }


   public function likes(){
       return $this->hasMany(Likes::class);
   }

   public function liker(){
        return $this->belongstoMany(User::class);
   }


  
}
