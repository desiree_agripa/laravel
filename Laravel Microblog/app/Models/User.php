<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'firstname',
        'middlename',
        'lastname',
        'email',
        'username',
        'password',
    ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected static function boot(){

        parent::boot();

        static::created(function($user){
            $user->profile()->create();

            
        });
    }

    public function profileImage(){
        
        $imagePath = ($this->photo) ? $this->photo : 'profile/ctUbi70dKCimfihlJeO1hc2nA5KigZO5Ayijhcie.png';
       return '/storage/' . $imagePath;
    }

    public function posts(){
        return $this->hasMany(Post::class)->orderBy('created_at','DESC');
    }

    public function following(){
        return $this->belongstoMany(Profile::class);
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function comments(){
        return $this->hasMany(comments::class);
    }

    public function shares(){
        return $this->hasMany(Shares::class);
    }

  // public function likes(){
  //       return $this->hasMany(Likes::class);
  // }


    public function profileBy(User $user){
        return $user->id === $this->user_id;
    }

    public function liking(){
        return $this->belongstoMany(Post::class);
    }

}
