@if($post->comments->count() != 0)
                <div class="card-body">
                <!-- Comment -->
                     @foreach($post->comments as $comment)
                     <div class="be-comment-block">
                         @if(auth()->user()->id == $comment->user->id)

                         <div class="be-comment">
                            <div class="be-img-comment">
                                @if ( $comment->user->photo == '')
                                <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  class="be-ava-comment">
                                @else
                                <img src="<?= asset('storage/'.$comment->user->photo) ?>"  class="be-ava-comment">
                                @endif
                            </div>
                            <div class="be-comment-content">
                            <div class="d-flex align-items-baseline">
                              <a href="/profile/<?= $comment->user->id ?>" class="addresslink"><?= $comment->user->firstname?>  <?=$comment->user->lastname ?></a>

                                <!-- Dropdown -->
                                <div class="dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#"  role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                        <!-- Edit -->
                                        <a class="dropdown-item">
                                            <button type="submit" class="lcsBtn" data-bs-toggle="modal"  data-bs-target="#ModalCommentEdit<?= $comment->id ?>"><i class="bi bi-pen"></i> Edit</button>
                                        </a>
                                        <!-- End Edit -->
                                        <!-- Delete -->
                                        <a class="dropdown-item">
                                        <button type="submit" style="border:none; background-color: transparent;" data-bs-toggle="modal"  data-bs-target="#ModalCommentDelete<?= $comment->id ?>"><i class="bi bi-trash"></i> Delete</button> 
                                        </a>
                                        <!-- End Delete -->
                                        </div>
                                       
                                            </div>
                                    <!-- End Dropdown -->
                                    <!-- Created at -->
                                        <span class="be-comment-time"><i class="fa fa-clock-o"></i><!-- Date here--> </span>
                                    <!-- End Created at -->
                                </div>
                                <label for="" style="font-size:10px; padding-top:-140px"><?= $comment->created_at->toDayDateTimeString();?></label>
                                <!-- Comment display -->
                                <p class="be-comment-text"><?= $comment->comment ?></p>
                                <!-- End Comment display -->
                               
                        </div>
                        </div> 
                         @include('modals.commentdelete')
                         @include('modals.commentedit')
                         @else
                         <div class="be-comment">
                            <div class="be-img-comment">	
                                <a href="">
                                    @if ( $comment->user->photo == '')
                                        <img  src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  class="be-ava-comment">
                                    @else
                                        <img  src="<?= asset('storage/'.$comment->user->photo) ?>"  class="be-ava-comment">
                                    @endif
                                </a>
                            </div>
                            <div class="be-comment-content">
                                    <div class="d-flex align-items-baseline">
                                        <a href="/profile/<?= $comment->user->id ?>" class="addresslink"><?= $comment->user->firstname?> <?=$comment->user->lastname?></a>
                                            <span class="be-comment-time">
                                                <i class="fa fa-clock-o"></i>
                                                <!-- Date here--> 
                                            </span>
                                    </div>
                                    <label for="" style="font-size:10px; padding-top:-140px"><?= $comment->created_at->toDayDateTimeString();?></label>
                                <!-- Comment display -->
                                <p class="be-comment-text">
                                    <?= $comment->comment ?>
                                </p>
                            </div>
                        </div>
                         @endif
                     </div>
                     @endforeach
                
                </div>
                @endif