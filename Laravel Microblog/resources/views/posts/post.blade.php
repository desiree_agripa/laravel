
<div class="container">
    <div class="row justify-content-center">
        <!-- Posts Section -->
        <div class="col-md-6">
                <div class="card" style="margin-bottom: 10px;">
                   <div class="card-body d-flex align-text-baseline">
                   <div>
                        @if ( auth()->user()->photo == '')
                        <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 35px; "alt="" class="rounded-circle">
                         @else
                         <img src="<?= asset('storage/'.auth()->user()->photo) ?>" alt=""  style="width: 35px; " class="rounded-circle">
                         @endif
                    </div>
                    <div style="padding-left: 10px">
                        <button type="button" class="input-group-text postBtn" data-bs-toggle="modal" style="width:550px; text-align: left" data-bs-target="#newPostModal">
                         What's on your mind, <?= auth()->user()->firstname ?>?  
                        </button>
                    </div>
                </div>
                </div>
            @if($posts->count())
             <!-- POST-->
             @foreach($posts as $post)
            <div class="card" style="margin-bottom: 10px;">
                <div class="card-body d-flex">
                    <div> 
                        @if ( $post->user->photo == '')
                        <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 35px; "alt="" class="rounded-circle">
                         @else
                         <img src="<?= asset('storage/'.$post->user->photo) ?>" alt=""  style="width: 35px; " class="rounded-circle">
                         @endif
                    </div>
                   <div style="padding-top: 5px; padding-left: 10px">
                        <div>
                        <a href="/profile/<?= $post->user->id ?>" class="addresslink"><b><?= $post->user->firstname;?> <?=  $post->user->lastname; ?></b></a><br>
                            <label for="" style="font-size:10px; padding-top:"><?= $post->created_at->diffForHumans();?></label>

                            <p><?= $post->description; ?></p>
                            @if($post->image != '')
                            <a href="/posts/<?= $post->id ?>"><img src="<?= asset('storage/'.$post->image) ?>" style="width: 100%; height: auto;"></img></a>
                            @else
                            @endif 
                            <!-- if shared post -->
                                @if (!empty($post->post_shared_id))
                                    @foreach($getAllPosts as $getAllPost)
                                        @if($getAllPost->id === $post->post_shared_id)
                                            <div style="padding-top: 5px; padding-left: 10px">
                                                    @if(!$getAllPost->image == '')  
                                                    <div class="sharephotoshare">
                                                        <img src="<?= asset('storage/'.$getAllPost->image) ?>" style="width: 100%; height: auto;"/>
                                                    </div>
                                                    @endif
                                                    </div>
                                                    <div style="padding-top: 5px; padding-left: 10px">
                                                        <div class="divbordershare">
                                                            <b><?= $getAllPost->user->firstname;?> <?= $getAllPost->user->lastname; ?></b><br>
                                                            <label style="font-size:10px; padding-top:"><?= $getAllPost->created_at->diffForHumans();?></label>
                                                            <p style="width: 100%;"><?= $getAllPost->description; ?></p>
                                                        </div>
                                                </div>
                                        @endif

                                    @endforeach
                                @else
                                @endif
                            <!-- if shared post -->
                        </div>
                        <div class="d-grid gap-2 d-md-block">
                            <span><strong><?=  $post->liker->count();?></strong> <?= Str::plural('like',  $post->liker->count()) ?></span>
                            <span><strong><?= $post->comments->count(); ?></strong> <?= Str::plural('comment', $post->comments->count()) ?></span>
                       
                        </div>
                   </div>
                </div>
                <div class="card-footer">
                    <div  class="d-flex gap-3"> 
                        <!--Like creation -->
                        <like-button post-id="<?= $post->id ?>" liked="<?= (auth()->user()) ? auth()->user()->liking->contains($post->id): false; ?>"></like-button>
                        <button type="button" class=" lcsBtn cancelbtn" data-bs-dismiss="modal">Comment</button>
                        <button type="button" class="lcsBtn cancelbtn" data-bs-toggle="modal"  data-bs-target="#ModalShare{{$post->id}}">Share</button>
                    </div>
                </div>
                    @include('modals.share')
                 <!--Comment creation -->
                   @include('posts.commentcreate')
                 <!--Comment creation -->
                    @include('posts.comments')
                <!-- Comment -->
            </div>  
              @endforeach
              <div class="row">
                  <div class="col-8 d-flex justify-content-center">
                      <?= $posts->links(); ?>
                  </div>
              </div>
              <!-- End-->
                @else
              <div class="card" style="margin-bottom: 10px;">
                   <div class="card-body d-flex">
                       No post yet.
                    </div>
                </div>
               @endif
            </div>
        <!--End POST-->
        @if(!$usersList->count() == 0)
        <!--start right-->
        <div class="col-md-3">
                <!-- People you might know section -->
                    <div class="card">
                    <div class="card-header">
                        People you might know 
                        
                        <a href="/follow" style="float:right; text-decoration: none;"> Show all</a> 
                        
                    </div>
                    <div class="card-body">
                        @foreach($usersList as $usersLists)
                            <div class="d-flex align-text-baseline">
                                            <div style="padding-bottom: 5px">
                                                @if ( $usersLists->photo == '')
                                                    <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 35px; " class="rounded-circle">
                                                @else
                                                    <img src="<?= asset('storage/'.$usersLists->photo) ?>"  style="width: 35px; " class="rounded-circle">
                                                @endif
                                            <a href="/profile/<?= $usersLists->id?>" class="addresslink"><?= $usersLists->firstname?> <?=$usersLists->lastname?></a>
                                            </div>
                                        </div>
                                        <div> </div>
                            @endforeach
                        </div>
                    </div>
                <!-- End people you might know section -->
         </div>
        <!--endright-->
        @endif
    </div>
</div>
@include('modals.create')