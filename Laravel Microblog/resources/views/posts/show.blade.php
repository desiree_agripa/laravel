@extends('layouts.app')
@include('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
    <div class="col-md-10">
            <!-- Profile Photo and Information -->
    <div class="row">
        <div class="card" style="margin-bottom: 20px;">
            <div class="card-body d-flex">
                <!-- Posts Section -->
                <div class="col-md-8">
                    <div>
                        <img src="/storage/<?=$post->image?>" alt="" width="100%">
                    </div>
                   
                </div>
                <div class="col-md-4">
                    <div class="d-flex align-items-center">
                            <div class="be-comment-block" style="padding-left: 20px">
                                @if( $post->photo == '')
                                <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg" class="rounded-circle w-100" style="max-width:40px">
                                @else
                                <img src="/storage/{{$post->user->photo}}" alt="" class="rounded-circle w-100" style="max-width:40px">
                                @endif 
                            </div>
                            <div>
                                    <div style="font-weight-bold; padding-left: 5px">
                                        <a href="/profile/<?= $post->user->id ?>"  style="text-decoration: none">
                                            <span class="text-dark">
                                                <strong><?= $post->user->firstname ?> <?= $post->user->lastname ?></strong>
                                            </span> 
                                            <span class="text-dark">
                                            <br>
                                            <label for="" style="font-size:10px; padding-top:-140px"><?= $post->created_at->toDayDateTimeString();?></label>
                                            </span>
                                        </a>
                                    </div>
                            </div>
                    </div>
                    <div class="be-comment-block" style="padding-left: 20px">
                        <span style="font-weight-bold">
                          <span class="text-dark">
                            <?= $post->description ?>
                          </span>
                        </span>

                    </div>
                    <div class="be-comment-block" style="padding-left: 20px">
                    <span><strong><?= $post->liker->count();?></strong> <?= Str::plural('like',  $post->liker->count()) ?></span>
                            <span><strong><?= $post->comments->count(); ?></strong> <?= Str::plural('comment', $post->comments->count()) ?></span>
                            
                    </div>
                    <hr>
                    <div class="be-comment-block-new d-flex gap-3">
                        <like-button post-id="<?= $post->id ?>" liked="<?= (auth()->user()) ? auth()->user()->liking->contains($post->id): false; ?>"></like-button>
                        <button type="button" class=" lcsBtn cancelbtn" data-bs-dismiss="modal">Comment</button>
                        <button type="button" class="lcsBtn cancelbtn" data-bs-toggle="modal"  data-bs-target="#ModalShare{{$post->id}}">Share</button>
                    </div>
                        @include('modals.share')
                        <hr>
                <!--Comment creation -->
                @include('posts.commentcreate')
                <!--Comment creation -->
                @include('posts.comments')
                <!-- Comment -->
                </div>

            </div>
        </div>
    </div>
    </div>
    </div>
</div>
@endsection
