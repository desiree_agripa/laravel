@extends('layouts.app')
@include('layouts.nav')
@section('content')
<div class="container">
    <form action="/posts" enctype="multipart/form-data" method="post">
        @csrf
        <div class="row justify-content-center">
         <div class="col-md-8">
            <div class="card" style="margin-bottom: 20px;">
                        <div class="card-header">
                            Create Post
                        </div>
                        <div class="card-body">
                            <div style="padding-top: 5px; padding-left: 10px">
                                <input type="text" id="description" class="form-control @error('description') is-invalid @enderror" size="100" name="description"  value="<?= old('description') ?>" style="border-radius: 10px;" placeholder="What's on your mind?">
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?= $message ?></strong>
                                    </span>
                                @enderror
                            </div>
                            <div style="padding-top: 5px; padding-left: 10px">
                                <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" size="100" style="border-radius: 10px;" placeholder="">
                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?= $message ?></strong>
                                    </span>
                                @enderror
                            </div>
                            
                        </div>
                            <div class="row mb-2">
                            <div class="col-md-6 offset-md-5">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('New Post') }}
                                </button>

                            </div>
                        </div>
                            
                    </div>
            </div>
        </div>
    </div>
    </form>
</div>
