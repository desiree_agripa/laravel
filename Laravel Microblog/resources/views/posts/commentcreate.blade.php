<div class="card-body">
    <form action="<?= route('comment.store'); ?>" method="post">
    @csrf
        <div class="d-flex align-text-baseline">
         <div>
            @if ( auth()->user()->photo == '')
            <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 35px; "alt="" class="rounded-circle">
            @else
            <img src="<?= asset('storage/'.auth()->user()->photo) ?>" alt=""  style="width: 35px; " class="rounded-circle">
            @endif
         </div>
         <div style="padding-left: 10px">
               <div class="d-flex"  class="inputField">
                     <input type="hidden" name="post_id" value="{{$post->id}}">
                    <input type="text" class="form-control postBtn " name="comment" size="100" placeholder="Write a comment...">
                    <button class="btn btn-default"  style="outline: none;" type="submit"> <i class="bi bi-send"></i></button>
                    
               </div>

          </div>
        </div>
    </form>
</div>
