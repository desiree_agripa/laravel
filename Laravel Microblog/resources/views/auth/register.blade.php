@extends('layouts.app')

@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
                <div class="row w-100">
                    <div class="col-lg-4 mx-auto">
                        <h2 class="text-center mb-4">{{ __('Register') }}</h2>
                        <div class="auto-form-wrapper">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form-group p-1">
                                    <div class="col-md-12">
                                    <input id="firstname" type="text" placeholder="Firstname" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" autofocus>
                                    @error('firstname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="form-group p-1">
                                    <div class="col-md-12">
                                    <input id="middlename" type="text" placeholder="Middlename" class="form-control @error('middlename') is-invalid @enderror" name="middlename" value="{{ old('middlename') }}" autofocus>
                                    @error('middlename')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="form-group p-1">
                                    <div class="col-md-12">
                                    <input id="lastname" type="text" placeholder="Lastname" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}"  autofocus>
                                    @error('lastname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="form-group p-1">
                                    <div class="col-md-12">
                                    <input id="email" type="email" placeholder="Email Address" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" >
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group p-1">
                                    <div class="col-md-12">
                                    <input id="username" type="text" placeholder="Username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" >
                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="form-group p-1">
                                    <div class="col-md-12">
                                    <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" name="password" >
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>
                                                {{ $message }} 
                                                <p class="passwordFont">  <br>
                                                Password must meet the following requirements: </br> 
                                                    * Minimum of 8 characters  </br> 
                                                    * Must contain at least one digit (0-9)   </br> 
                                                    * Must contain at least one upper case  letter (A-Z)  </br> 
                                                    * Must contain at least one lowercase case (a-z)  </br> 
                                                    * Must contain at least one special character (!@#$%^&)  
                                                </p>
                                            </strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="form-group p-1">
                                    <div class="col-md-12">
                                    <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation">
                                    </div>
                                </div>
                                
                                <div class="form-group p-1">
                                    <button type="submit" class="btn btn-primary submit-btn btn-block">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                                <div class="text-block text-center my-3  p-1">
                                    <span class="text-small font-weight-semibold">Already have an account ?</span>
                                    <a href="{{ route('login') }}" class="text-black text-small">{{ __('Login') }}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
@endsection