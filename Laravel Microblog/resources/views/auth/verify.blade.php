@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Account') }}</div>

                <div class="card-body justify-content-center">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    {{ __('Thank you for signing up!') }} <br>
                    {{ __('Before getting started, please verify your email address by clicking on the link we just emailed you.If you did not receive the email, we will gladly send you another.') }}<br>
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <br><button type="submit" class="btn btn-primary btn-sm align-baseline">{{ __(' Resend verification email ') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
