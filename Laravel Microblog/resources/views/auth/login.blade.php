@extends('layouts.app')
<link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
                <div class="row w-100">
                    <div class="col-lg-4 mx-auto">
                    <h2 class="text-center mb-4">{{ __('Login') }}</h2>
                        <div class="auto-form-wrapper">
                        @if(session('status'))
                            <div>
                                <?= session('status') ?>
                            </div>
                            @endif
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group p-1">
                                    <label class="label">Email</label>
                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"   autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                            

                                </div>
                                <div class="form-group p-1">
                                    <label class="label">Password</label>
                                  <div class="col-md-12">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" name="password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                </div>
                                <div class="form-group p-1">
                                    <button type="submit" class="btn btn-primary submit-btn btn-block">
                                        {{ __('Login') }}
                                    </button>

                                </div>
                                <div class="text-block text-center my-3">
                                    <span class="text-small font-weight-semibold">No account yet ?</span>
                                    <a href="{{ route('register') }}" class="text-black text-small">Create new account</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <script src="{{ asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('assets/js/shared/misc.js') }}"></script>
@endsection