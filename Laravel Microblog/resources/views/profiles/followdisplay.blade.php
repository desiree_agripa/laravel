
               
                <div class="col-4" style="padding-top:20px;">
                  <!-- right side start -->
                   <!-- People you follow section -->
                    <div class="card">
                    <div class="card-header">
                         <b>Following</b> 
                         @if(!$followingCount == '0')
                         <a href="/following/<?= $user->id ?>" class="follow"> See all following</a> 
                         @endif
                         <div>
                            <label style="font-size:13px;">
                                <?= $followingCount; ?></strong > <?= Str::plural('following', $followingCount) ?>
                            </label>
                         </div>
                    </div>
                    @if(!$followingCount == '0')
                    <div class="card-body">
                        @foreach($usersFollowing as $userFollow)
                                    <div class="d-flex align-text-baseline">
                                        <div style="padding-bottom: 5px">
                                            @if ( $userFollow->user->photo == '')
                                                <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 30px; " class="rounded-circle">
                                            @else
                                                <img src="{{ asset('storage/'.$userFollow->user->photo) }}"  style="width: 30px; " class="rounded-circle">
                                            @endif
                                        <a href="/profile/<?= $userFollow->id ?>" class="addresslink"><?= $userFollow->user->firstname?> <?= $userFollow->user->lastname?></a>
                                        </div>
                                    </div>
                                    <div> </div>
                            @endforeach
                        </div>
                    @endif
                    </div>
                <!-- end People you follow section -->
                <!-- Follower section -->
                <div class="card">
                        <div class="card-header"> 
                            <b>Followers</b> 
                            @if(!$followerCount == '0')
                            <a href="/followers/<?= $user->id ?>" class="follow"> See all followers</a> 
                            @endif
                            <div>
                            <label style="font-size:13px;">
                                <?= $followerCount; ?></strong > <?= Str::plural('follower', $followerCount) ?>
                            </label>
                         </div>
                        </div>
                        @if(!$followerCount == '0')
                        <div class="card-body">
                                 @foreach($usersFollowers as $userFollower)
                                <div class="d-flex align-text-baseline">

                                        <div style="padding-bottom: 5px">
                                             @if ( $userFollower->profile->user->photo == '')
                                                <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 30px; " class="rounded-circle">
                                            @else
                                                <img src="{{ asset('storage/'.$userFollower->profile->user->photo) }}"  style="width: 30px; " class="rounded-circle">
                                            @endif
                                        <a href="/profile/<?= $userFollower->id?>" class="addresslink"><?= $userFollower->profile->user->firstname?> <?= $userFollower->profile->user->lastname?></a>  
                                        </div>   
                                </div>
                                @endforeach
                            </div>
                        @endif
                        </div>
                    <!-- end Follower section -->
                   <!--right side end-->
                </div>
