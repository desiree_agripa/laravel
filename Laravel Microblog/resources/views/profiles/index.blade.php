@extends('layouts.app')
@include('layouts.nav')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <!-- Posts Section -->
        <div class="col-md-8">
            <!-- Profile Photo and Information -->
            <div class="row">
                <div class="col-3" style="padding:20px">
                    @if ( $user->photo == '')
                    <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg" class="rounded-circle w-100">
                    @else
                        <img src="<?= $user->profileImage() ?>" alt="" class="rounded-circle w-100">
                    @endif
                </div>
                <div class="col-9" style="padding:30px">
                    <div class="flex justify-content-between align-items-baseline">
                        <div class="d-flex align-items-center">
                        <h3><?= $user->firstname; ?> <?= $user->lastname; ?></h3> 
                            <div class="align-items-baseline">
                                @if(auth()->user()->id!=$user->id)
                                <follow-button user-id="<?= $user->id ?>" follows="<?= $follows ?>"></follow-button>
                                @endif
                            </div>
                        </div>
                        <h5 style="padding-top: -10px">@<?= $user->username; ?></h5>
                    </div>
                    
                    <div class="d-flex align-items-baseline">
                        <div style="padding-right:20px"><strong><?= $postCount; ?></strong> <?= Str::plural('post', $postCount) ?></div>
                        <div style="padding-right:20px">
                            @if(!$followerCount == 0)
                            <a href="/followers/<?= $user->id ?>" class="addresslink"><strong><?= $followerCount; ?></strong> <?= Str::plural('follower', $followerCount) ?></strong></a>
                            @else
                            <strong><?= $followerCount; ?></strong > <?= Str::plural('follower', $followerCount) ?></strong>
                            @endif
                        </div>
                        <div style="padding-right:20px">
                            @if(!$followingCount == 0)
                            <a href="/following/<?= $user->id ?>" class="addresslink"><strong><?= $followingCount; ?></strong > <?= Str::plural('following', $followingCount) ?></strong></a>
                            @else
                            <strong><?= $followingCount; ?></strong > <?= Str::plural('following', $followingCount) ?></strong>
                            @endif
                        </div>
                            @can ('update', $user->profile)
                            <!-- <div style="padding-right:5px; padding-left: 120px"><a href="/posts/create" class="btn btn-primary btn-sm">New Post</a></div> -->
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-backdrop="static"   data-bs-target="#newPostModal">
                            <i class="bi bi-plus-circle"></i> New Post
                            </button>
                            <button type="button" class="btn btn-default btn-sm profileBtn" data-bs-toggle="modal" data-backdrop="static"   data-bs-target="#updateProfileModal">
                            <i class="bi bi-pencil-fill"></i> Update Profile
                            </button>
                            @endcan
                    </div>
                </div>
            </div>
        </div>
        @include('modals.editprofile')
        <div class="col-md-10">
            <div class="row justify-content-center">
                <div class="col-8" style="padding:20px">

            @if($user->posts->count())
                  <!-- left side start -->
                     <!-- POST-->
              
    @foreach($posts as $post)
            <div class="card" style="margin-bottom: 10px;">
                <div class="card-body d-flex">
                    <div>
                         @if ( $post->user->photo == '')
                        <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 35px; " class="rounded-circle">
                         @else
                         <img src="{{ asset('storage/'.$post->user->photo) }}"  style="width: 35px; " class="rounded-circle">
                         @endif
                    </div>
                   <div style="padding-left: 10px">
                        <div>
                            @if($post->ownedBy(auth()->user()))
                            <div class="d-flex align-items-baseline">
                                <label for=""><b><?= $post->user->firstname;?> <?=  $post->user->lastname; ?></b></label>
                                <div class="dropdown" style="float: right">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle align-right" href="#"  role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                     <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item">
                                                <button type="submit" class="editdelete" data-bs-toggle="modal"  data-bs-target="#ModalEdit<?= $post->id ?>"><i class="bi bi-pen"></i> Edit</button>
                                            </a>
                                            <a class="dropdown-item">
                                            <button type="submit" class="editdelete" data-bs-toggle="modal"  data-bs-target="#ModalDelete<?= $post->id ?>"><i class="bi bi-trash"></i> Delete</button> 
                                            </a>
                                     </div>
                                </div>
                            </div>
                            <label for="" style="font-size:10px; padding-top:-140px"><?= $post->created_at->toDayDateTimeString();?></label>
                            @include('modals.edit')
                            @include('modals.delete') 
                            @else
                                <div class="flex align-items-baseline">
                                    <label for=""><b><?= $post->user->firstname;?> <?=  $post->user->lastname; ?></b></label>
                                </div>
                                
                                <label for="" style="font-size:10px; padding-top:-140px"><?= $post->created_at->toDayDateTimeString();?></label>
                            @endif
                           
                        
                            <p><?= $post->description; ?></p>
                            
                            @if($post->image != '')
                            <a href="/posts/<?= $post->id ?>"><img src="<?= asset('storage/'.$post->image) ?>" style="width: 100%; height: auto;"></img></a>
                            @else
                            @endif 
                              <!-- if shared post -->
                              @if (!empty($post->post_shared_id))
                                    @foreach($getAllPosts as $getAllPost)
                                        @if($getAllPost->id === $post->post_shared_id)
                                            <div style="padding-top: 5px; padding-left: 10px">
                                                    @if(!$getAllPost->image == '')  
                                                    <div class="sharephotoshare">
                                                        <img src="<?= asset('storage/'.$getAllPost->image) ?>" style="width: 100%; height: auto;"/>

                                                    </div>
                                                    @endif
                                                    </div>
                                                    <div style="padding-top: 5px; padding-left: 10px">
                                                        
                                                        <div class="divbordershare">
                                                            <b><?= $getAllPost->user->firstname;?> <?= $getAllPost->user->lastname; ?></b><br>
                                                            
                                                            <label style="font-size:10px; padding-top:"><?= $getAllPost->created_at->toDayDateTimeString();?></label>
                                                            <p style="width: 100%;"><?= $getAllPost->description; ?></p>
                                                        </div>
                                                </div>
                                        @endif

                                    @endforeach
                                @else
                                @endif
                            <!-- if shared post -->
                        </div>
                        <div class="d-grid gap-2 d-md-block">
                            <span><strong><?= $post->liker->count(); ?></strong> <?= Str::plural('like', $post->liker->count()) ?></span>
                            <span><strong><?= $post->comments->count(); ?></strong> <?= Str::plural('comment', $post->comments->count()) ?></span>
                        </div>
                   </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex gap-3">
                        <like-button post-id="<?= $post->id ?>" liked="<?= (auth()->user()) ? auth()->user()->liking->contains($post->id): false; ?>"></like-button>
                        <button type="button" class=" lcsBtn cancelbtn" data-bs-dismiss="modal">Comment</button>
                        <button type="button" class="lcsBtn cancelbtn" data-bs-toggle="modal"  data-bs-target="#ModalShare{{$post->id}}">Share</button>
                    </div>
                </div>
                    @include('modals.share')
                        <!--Comment creation -->
                    @include('posts.commentcreate')
                        <!--Comment creation -->
                    @include('posts.comments')
                        <!-- Comment -->
              </div>  
              @endforeach
              <!--End POST-->
              <div class="row justify-content-center">
                  <div class="col-8 d-flex justify-content-center">
                      <?= $posts->links(); ?>
                  </div>
              </div>
    
              <!--End POST-->
             
              <!--left side end-->
                    @else
                    <div class="card" style="margin-bottom: 10px;" >
                        <div class="card-body d-flex ">
                            No post yet.
                            </div>
                        </div>
                    @endif
                </div>
             @include('profiles.followdisplay')
            </div>
     </div>
        
    </div>
</div>
@include('modals.create')

@endsection

@yield('scripts')
@section('scripts')
<script>
    var loadFile = function(event){
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.style.width = '450px';
    output.style.height = 'auto';
  };

$(document).ready(function(){

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      }); 
  // Edit Profile
  $(document).on('submit', '#updateProfileForm',function(e){
    e.preventDefault();

    var $userID = $('#userID').val();
    let updateFormData = new FormData($('#updateProfileForm')[0]);

    $.ajax({
       type: "POST",
       url: "/profile/update/"+ $userID,
       data: updateFormData,
       contentType: false,
       processData: false,
       success: function(response){
        if(response.status == 400)
         {
            $('#updateform_errList').html("");
            $('#updateform_errList').removeClass('d-none');
            $.each(response.errors, function(key, err_values){
              $('#updateform_errList').append('<li>'+err_values+'</li>');
            });
         }else if(response.status == 404){

            swal.fire({
              title: "Post not Found!",
              text: response.message,
              type: "warning",
              showConfirmButton: false,
              timer: 2000
           });
            
         }else if(response.status == 200){
            $('#updateform_errList').html("");
            $('#updateform_errList').addClass('d-none');
            $('#updatePostModal').find('input').val("");
            $('#updateProfileForm').find('img').val("");
            $('#updateProfileForm').modal('hide');
            window.location = "/profile/<?= auth()->user()->id?>";
            
            swal.fire({
              title: "Profile successfully updated!",
              text: response.message,
              type: "success",
              showConfirmButton: false,
              timer: 2000
           });
            
         }
       }
    });
  });
  // End Profile Edit


   // Comment Edit
   $(document).on('submit', '#updateCommentModal',function(e){
    e.preventDefault();

    var $commentID = $('#comment_id').val();
    let updateFormData = new FormData($('#updateCommentModal')[0]);

    $.ajax({
       type: "POST",
       url: "/comment/update/"+ $commentID,
       data: updateFormData,
       contentType: false,
       processData: false,
       success: function(response){
        if(response.status == 400)
         {
            $('#updatecommentform_errList').html("");
            $('#updatecommentform_errList').removeClass('d-none');
            $.each(response.errors, function(key, err_values){
              $('#updatecommentform_errList').append('<li>'+err_values+'</li>');
            });
         }else if(response.status == 200){
            $('#updatecommentform_errList').html("");
            $('#updatecommentform_errList').addClass('d-none');
            $('#updateCommentModal').find('input').val("");
            $('#updateCommentModal').modal('hide');
            window.location = "/";
            
            swal.fire({
              title: "Comment successfully updated!",
              text: response.message,
              type: "success",
              showConfirmButton: false,
              timer: 2000
           });
            
         }
       }
    });
  });
   // End Comment Edit




  //Post Edit
  $(document).on('submit', '#updatePostForm',function(e){
    e.preventDefault();

    var $postID = $('#post_id').val();
    let updateFormData = new FormData($('#updatePostForm')[0]);
    console.log($postID);
    $.ajax({
       type: "POST",
       url: "/posts/update/"+ $postID,
       data: updateFormData,
       contentType: false,
       processData: false,
       success: function(response){

         if(response.status == 400)
         {
            $('#errors').html("");
            $('#errors').removeClass('d-none');
            $.each(response.errors, function(key, err_values){
              $('#errors').append('<li>'+err_values+'</li>');
            });
         }else if(response.status == 200){
            $('#errors').html("");
            $('#errors').addClass('d-none');
            $('#ModalEdit').find('input').val("");
            $('#ModalEdit').find('img').val("");
            $('#ModalEdit').modal('hide');
            window.location = "/profile/<?= auth()->user()->id?>";
           
            swal.fire({
              title: "Post updated!",
              text: response.message,
              type: "success",
              showConfirmButton: false,
              timer: 2000
           });
            
         }

       }

    });

  });




  // End Post Edit
 //Post create
  $(document).on('submit','#postForm', function(e){
      e.preventDefault();
    
      let formData = new FormData($('#postForm')[0]);

     $.ajax({
       type: "POST",
       url: "/posts",
       data: formData,
       contentType: false,
       processData: false,
       success: function(response){
         //console.log(response);
         if(response.status == 400)
         {
            $('#saveform_errList').html("");
            $('#saveform_errList').removeClass('d-none');
            $.each(response.errors, function(key, err_values){
              $('#saveform_errList').append('<li>'+err_values+'</li>');
            });
         }else if(response.status == 200){
            $('#saveform_errList').html("");
            $('#saveform_errList').addClass('d-none');
            $('#newPostModal').find('input').val("");
            $('#newPostModal').find('img').val("");
            $('#newPostModal').modal('hide');
            window.location = "/";
            //routeProfilePost();

            swal.fire({
              title: "Post created!",
              text: response.message,
              type: "success",
              showConfirmButton: false,
              timer: 2000
           });
            
         }
       } 
     });

  }); 
  //End Post create
});
</script>
@endsection 





