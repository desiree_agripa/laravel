@extends('layouts.app')
@include('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
    <div class="col-6" style="padding-top:20px">
        <!-- Follower section -->
        <div class="card">
                        <div class="card-header">
                            <div class=" d-flex">
                                <label for=""> Followers</label>  
                            </div>
                        </div>
                        <div class="card-body">

                        <div class="row">
                            @foreach($listFollowers as $listFollower)
                                <div class="col-6 pb-4">
                                    <div class="row">
                                    <div class="d-flex">
                                    <div> 
                                    @if ( $listFollower->profile->user->photo == '')
                                        <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"   class="followPicture">
                                    @else
                                        <img src="<?= asset('storage/'.$listFollower->profile->user->photo) ?>" alt=""  class="followPicture">
                                    @endif
                                    </div>
                                    <div style="padding-top: 5px; padding-left: 10px">
                                    
                                            <a href="/profile/<?=$listFollower->profile->user->id ?>" class="addresslink"><b><?= $listFollower->profile->user->firstname; ?> <?= $listFollower->profile->user->lastname; ?></b></a><br>
                                            <label for="" style="font-size:12px; padding-top:">@<?= $listFollower->profile->user->username; ?></label>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                            @endforeach
                       


                        </div>
                               
        </div>
        <!-- end Follower section -->
        </div>
    </div>
</div>
@endsection

