@extends('layouts.app')
@include('layouts.nav')
@section('content')
<div class="container">
    <!-- People section -->
    @if($searchedPeople->count() != 0)
    <div class="row justify-content-center">
        <div class="col-6" style="padding-top:20px">
            <div class="card">
            <div class="row p-4">
               @foreach($searchedPeople as $people)
                <div class="col-6">
                  <div class="row">
                      <div class="d-flex">
                        <div> 
                        @if ( $people->photo == '')
                            <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"   class="followPicture">
                        @else
                            <img src="<?= asset('storage/'.$people->photo) ?>" alt=""  class="followPicture">
                        @endif
                        </div>
                        <div style="padding-top: 5px; padding-left: 10px">
                          
                                <a href="/profile/<?= $people->id ?>" class="addresslink"><b><?= $people->firstname;?> <?=  $people->lastname; ?></b></a><br>
                                <label for="" style="font-size:12px; padding-top:">@<?= $people->username?></label>

                        </div>
                        </div>
                    </div>
                  </div>
                            @endforeach
               </div>
            </div>
                <!-- end People section -->
            </div>
        </div>
            @else
            <div class="row justify-content-center">
                <div class="col-6" style="padding-top:20px">
                    <div class="card">
                        <div class="card-header">
                            No User found.
                        </div>
                    </div>
                </div>    
            </div>
            @endif
        
    </div>
    <div class="container">
    @if($searchedPost->count() != 0)
    @foreach($searchedPost as $post)
    <div class="row justify-content-center">
        <div class="col-6" style="padding-top:20px">
            <!-- Post section -->
            <div class="card">
               <div class="card-body d-flex">
                    <div style="padding-top: 5px;">
                        @if ( $post->user->photo == '')
                        <img src="{{ asset('storage/profile/default-profile.jpg') }}"  style="width: 35px; "alt="" class="rounded-circle">
                         @else
                         <img src="{{ asset('storage/'.$post->user->photo) }}" alt=""  style="width: 35px; " class="rounded-circle">
                         @endif
                    </div>
                   <div style="padding-top: 5px; padding-left: 10px">
                        <div>
                            <a href="/profile/<?= $post->user->id ?>" class="addresslink"><b><?= $post->user->firstname;?> <?=  $post->user->lastname; ?></b></a><br>
                            <label for="" style="font-size:10px; padding-top:"><?= $post->user->created_at->toDayDateTimeString();?></label>

                            <p><?= $post->description; ?></p>
                            @if($post->image != '')
                            <a href="/posts/<?= $post->id ?>"><img src="<?= asset('storage/'.$post->image) ?>" style="width: 100%; height: auto;"></img></a>
                            @else
                            @endif 
                            <!-- if shared post -->
                                @if (!empty($post->post_shared_id))
                                    @foreach($getAllPosts as $getAllPost)
                                            @if($getAllPost->id === $post->post_shared_id)
                                                <div style="padding-top: 5px; padding-left: 10px">
                                                        @if(!$getAllPost->image == '')  
                                                        <div class="sharephotoshare">
                                                            <img src="{{ asset('storage/'.$getAllPost->image) }}" style="width: 100%; height: auto;"/>
                                                        </div>
                                                        @endif
                                                        </div>
                                                        <div style="padding-top: 5px; padding-left: 10px">
                                                            <div class="divbordershare">
                                                                
                                                                <b><?= $getAllPost->user->firstname;?> <?= $getAllPost->user->lastname; ?></b><br>
                                                                <label style="font-size:10px; padding-top:"><?= $getAllPost->created_at->toDayDateTimeString();?></label>
                                                                <p style="width: 100%;"><?= $getAllPost->description; ?></p>
                                                            </div>
                                                    </div>
                                            @endif 

                                        @endforeach
                                @else
                                @endif
                            <!-- if shared post -->
                        </div>
                        <div class="d-grid gap-2 d-md-block">
                        <span><strong><?= $post->liker->count(); ?></strong> <?= Str::plural('like', $post->liker->count()) ?></span>
                            <span><strong><?= $post->comments->count(); ?></strong> <?= Str::plural('comment', $post->comments->count()) ?></span>
                        </div>
                        
                   </div>
                
             </div> 
             <div class="card-footer">
                    <div class="d-flex gap-3">
                        <like-button post-id="<?= $post->id ?>" liked="<?= (auth()->user()) ? auth()->user()->liking->contains($post->id): false; ?>"></like-button>
                        <button type="button" class=" lcsBtn cancelbtn" data-bs-dismiss="modal">Comment</button>
                        <button type="button" class="lcsBtn cancelbtn" data-bs-toggle="modal"  data-bs-target="#ModalShare{{$post->id}}">Share</button>
                    </div>
                </div>
                    @include('modals.share')
                        <!--Comment creation -->
                    @include('posts.commentcreate')
                        <!--Comment creation -->
                    @include('posts.comments')
                        <!-- Comment -->
            <!-- end Post section -->
        </div>
        </div>
    </div>
    @endforeach
    @else
    <div class="row justify-content-center">
        <div class="col-6" style="padding-top:20px">
            <div class="card">
               <div class="card-header">
                   No Post found.
               </div>
            </div>
        </div>    
    </div>
    @endif
</div>
</div>
@endsection

