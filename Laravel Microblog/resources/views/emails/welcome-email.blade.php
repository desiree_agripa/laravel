@component('mail::message')
# Welcome

Thank you for signing up for Microblog!

Please verify your email address by clicking the button below.

@component('mail::button', ['url' => '127.0.0.8000/'])
Confirm my account
@endcomponent

Yours,<br>
Desiree
@endcomponent
