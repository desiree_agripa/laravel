<form method="post" action="/comment/<?= $comment->id; ?>">
   @csrf
   @method('DELETE')
   <div class="modal fade" id="ModalCommentDelete<?= $comment->id ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Are you sure you want to delete this comment?</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <button type="submit" class="btn btn-primary deletebtn ">Delete</button>
        <button type="button" class="btn btn-secondary cancelbtn" data-bs-dismiss="modal">Cancel</button> 
      </div>
    </div>
  </div>
</div>
</form> 