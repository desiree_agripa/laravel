<!-- Modal --> 
<div class="modal fade" id="newPostModal"  aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
<form id="postForm" method="POST"  enctype="multipart/form-data">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Post</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <div class="d-flex align-text-baseline" style="padding-right: 10px">
        <div>
         @if ( auth()->user()->photo == '')
            <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 40px; "alt="" class="rounded-circle">
         @else
             <img src="<?= asset('storage/'.auth()->user()->photo) ?>" alt=""  style="width:40px; " class="rounded-circle">
         @endif
         </div>
      <div style="padding-top: 5px; padding-left: 10px">
        <div>
          <label><b><?= auth()->user()->firstname;?> <?=  auth()->user()->lastname; ?></b></label><br>
          <label for="" style="font-size:10px; padding-top:"><?= date('F d, Y h:i:s');?></label>
        </div>
      </div>
      </div>
      <div style="padding-top: 5px; padding-left: 10px">
          <ul class="alert alert-danger d-none" id="saveform_errList"></ul>
         <input type="text" id="description" class="form-control" size="100" name="description"  style="border-radius: 10px;" placeholder="What's on your mind, <?= auth()->user()->firstname; ?>?">         
      </div>
      <div style="padding-top: 5px; padding-left: 10px">
      <input type="file" class="form-control" id="image" name="image" size="100" style="border-radius: 10px;" placeholder="" onchange="loadFile(event)">
              </div>
              <div style="padding-top: 5px; padding-left: 10px">
              <img id="output"/>
              </div>
       <div>
         
        
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Post</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
   
    </div>
  </div> 
  </form>
</div>

