
<!-- Modal --> 
<form method="post" id="updateCommentModal"  enctype="multipart/form-data">
    <div class="modal fade" id="ModalCommentEdit<?=$comment->id?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Comment</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <div style="padding-top: 5px; padding-left: 10px">
         <ul class="alert alert-danger d-none" id="updatecommentform_errList"></ul>
         <input type="hidden" value="<?= $comment->id ?>" id="comment_id">
         <input type="text" class="form-control @error('comment') is-invalid @enderror" id="comment" size="100" name="comment"  value="<?= $comment->comment; ?>" style="border-radius: 10px;" placeholder="Write a comment">         
      </div>
       <div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Edit</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>