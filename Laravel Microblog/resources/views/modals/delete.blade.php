<form method="post" action="/posts/<?= $post->id; ?>">
   @csrf
   @method('DELETE')
   <div class="modal fade" id="ModalDelete<?= $post->id ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Are you sure you want to delete this post?</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div style="padding-left: 10px">
           @if(!$post->image == '')
          <div>
             <img src="<?= asset('storage/'.$post->image) ?>" width="430px"/>
          </div>
          @endif
       </div>
       <div class="d-flex align-text-baseline" style="padding-top: 5px; padding-left: 10px">
            <div>
              @if (auth()->user()->photo == '')
              <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 35px; "alt="" class="rounded-circle">
              @else
              <img src="<?= asset('storage/'.auth()->user()->photo) ?>" alt=""  style="width: 35px; " class="rounded-circle">
              @endif
            </div>
          <div style="padding-top: 5px; padding-left: 10px">
            <b><?= $post->user->firstname;?> <?=  $post->user->lastname; ?></b><br>
            <label for="" style="font-size:10px; padding-top:"><?= $post->user->created_at->diffForHumans();?></label>
            <p><?= $post->description; ?></p>
          </div>
          
          <hr> 
      </div>
        <div style="padding-top: 5px; padding-left: 30px">
           <!-- if shared post -->
           @if (!empty($post->post_shared_id))
             @foreach($getAllPosts as $getAllPost)
             @if($getAllPost->id === $post->post_shared_id)
                <div style="padding-top: 5px; padding-left: 10px">
                 @if(!$getAllPost->image == '')  
                  <div class="sharephotoshare">
                    <img src="<?= asset('storage/'.$getAllPost->image) ?>" style="width: 100%; height: auto;"/>
                  </div>
                 @endif
                </div>
                <div style="padding-top: 5px; padding-left: 10px">
                  <div class="divbordershare">
                    <b><?= $getAllPost->user->firstname;?> <?= $getAllPost->user->lastname; ?></b><br>
                      <label style="font-size:10px; padding-top:"><?= $getAllPost->created_at->diffForHumans();?></label>
                       <p style="width: 100%;"><?= $getAllPost->description; ?></p>
                   </div>
                </div>
            @endif
            @endforeach
           @endif
                            <!-- if shared post -->
        </div>
        <button type="submit" class="btn btn-primary deletebtn ">Delete</button>
        <button type="button" class="btn btn-secondary cancelbtn" data-bs-dismiss="modal">Cancel</button>
    
        
      </div>
    </div>
  </div>
</div>
</form> 