
<!-- Modal --> 
<form  method="post" id="updateProfileForm" enctype="multipart/form-data">

  <div class="modal fade" id="updateProfileModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Update Profile</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row mb-3">
            <ul class="alert alert-danger d-none" id="updateform_errList"></ul>
        </div>
      <div class="row mb-3">
         <input type="hidden" value="<?= auth()->user()->id ?>" id="userID">
         <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Profile Photo') }}</label>
            <div class="col-md-3">
                @if ($user->photo == '')
                    <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg" id="output" class="rounded-circle w-100">
                @else
                    <img src="{{ asset('storage/'.$user->photo) }}" alt="" id="output"  class="rounded-circle w-100">
                @endif
                    <input type="file" id="photo" name="photo" value="<?= $user->photo ?>" onchange="loadFile(event)">
            </div>
      </div>
      <div class="row mb-3">
         <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Firstname') }}</label>
            <div class="col-md-6">
            <input id="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') ?? $user->firstname }}" autocomplete="firstname" autofocus>
            </div>
       </div>
       <div class="row mb-3">
         <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Middlename') }}</label>
            <div class="col-md-6">
            <input id="middlename" type="text" class="form-control @error('middlename') is-invalid @enderror" name="middlename" value="{{ old('middlename') ?? $user->middlename }}" autocomplete="middlename" autofocus>
            </div>
       </div> 
       <div class="row mb-3">
          <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Lastname') }}</label>
            <div class="col-md-6">
            <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') ?? $user->lastname}}" autocomplete="lastname" autofocus>
            </div>
       </div>
       <div class="row mb-3">
        <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>
            <div class="col-md-6">
            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? $user->email}}" autocomplete="email" disabled>
            </div>
       </div>
        <div class="row mb-3">
            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Username') }}</label>
            <div class="col-md-6">
            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') ?? $user->username}}" autofocus>
            </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>
<script>
  var loadFile = function(event){
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.style.width = '400px';
    output.style.height = 'auto';
  };
</script>