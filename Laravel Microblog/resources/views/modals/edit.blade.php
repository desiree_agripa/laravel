
<!-- Modal -->  
<form  method="post" id="updatePostForm" enctype="multipart/form-data">
  <div class="modal fade" id="ModalEdit<?= $post->id ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Post</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
          <div class="d-flex align-text-baseline">
            <div>
              @if (auth()->user()->photo == '')
              <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 35px; "alt="" class="rounded-circle">
              @else
              <img src="<?= asset('storage/'.auth()->user()->photo) ?>" alt=""  style="width: 35px; " class="rounded-circle">
              @endif
            </div>
            <div style="padding-top: 5px; padding-left: 10px">
                <h4><?= $post->user->firstname; ?> <?= $post->user->lastname; ?></h4>
            </div>
          </div>
      <div style="padding-top: 5px; padding-left: 10px">
      <ul class="alert alert-danger d-none" id="errors"></ul>
      <input type="hidden" value="<?= $post->id ?>" id="post_id">
      <input type="text" id="description" class="form-control @error('description') is-invalid @enderror" size="100" name="description"  value="{{ $post->description; }}" style="border-radius: 10px;" placeholder="What's on your mind, <?= $post->user->firstname; ?> ?">         
      </div>
        <div style="padding-top: 5px; padding-left: 10px">
         <input type="file" class="form-control" id="image" name="image" size="100" style="border-radius: 10px;" placeholder="" onchange="loadFile(event)">
       </div>
        <div style="padding-top: 5px; padding-left: 10px">
        @if(!$post->image == '')
          <img id="output" src="<?= asset('storage/'.$post->image) ?>" width="450px"/>
          @endif
          <img id="output" width="450px"/>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Edit Post</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>