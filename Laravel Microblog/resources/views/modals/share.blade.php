
<!-- Modal -->
<div class="modal fade" id="ModalShare<?=$post->id?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<form action="<?=route('posts.share')?>" method="post" enctype="multipart/form-data">
<?= csrf_field() ?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Share Post</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <div class="d-flex align-text-baseline" style="padding-right: 10px">
        <div>
         @if ( auth()->user()->photo == '')
            <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 40px; "alt="" class="rounded-circle">
         @else
             <img src="<?= asset('storage/'.auth()->user()->photo) ?>" alt=""  style="width:40px; " class="rounded-circle">
         @endif
         </div>
      <div style="padding-top: 5px; padding-left: 10px">
        <div>
          <label><b><?= auth()->user()->firstname;?> <?=  auth()->user()->lastname; ?></b></label><br>
          <label for="" style="font-size:10px; padding-top:"><?= date('F d, Y h:i:s');?></label>
        </div>
      </div>
      </div>
      <div style="padding-top: 5px; padding-left: 10px">
         <input type="hidden" name="post_id" value="<?= $post->id ?>">
         <input type="hidden" name="user_id" value="<?= auth()->user()->id ?>">
         <input type="text" id="description" class="form-control @error('description') is-invalid @enderror" size="100" name="description"  value="<?= old('description') ?>" style="border-radius: 10px;" placeholder="What's on your mind, <?= auth()->user()->firstname; ?>?">         
            @error('description')
              <span class="invalid-feedback" role="alert">
                <strong><?= $message ?></strong>
              </span>
            @enderror
      </div>
      <div style="padding-top: 5px; padding-left: 10px">
          <div class="sharephoto">
              @if(!$post->image == '')
              <img src="<?= asset('storage/'.$post->image) ?>" width="430px"/>
              @endif
          </div>
       </div>
       <div style="padding-top: 5px; padding-left: 10px" class="d-flex">
          <div style=" padding-right: 5px">
          @if ( $post->user->photo == '')
              <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 35px; "alt="" class="rounded-circle">
          @else
              <img src="<?= asset('storage/'.$post->user->photo) ?>" alt=""  style="width:35px; " class="rounded-circle">
          @endif
          </div>
          <div class="divborder">
            <b><?= $post->user->firstname;?> <?=  $post->user->lastname; ?></b><br>
            <label for="" style="font-size:10px; padding-top:"><?= $post->user->created_at->toDayDateTimeString();?></label>
            <p><?= $post->description; ?></p>
          </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Share Now</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  </form>
</div>

