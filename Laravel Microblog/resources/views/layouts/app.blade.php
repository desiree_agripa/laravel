<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Microblog') }}</title>
    <link href="https://media.glassdoor.com/sqll/1960738/yns-squarelogo-1533819178590.png" rel="icon" type="image/png">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- croptool -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{ asset('plugins/ijaboCropTool/ijaboCropTool.min.css')}}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{asset('plugins/ijaboCropTool/ijaboCropTool.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.15.3/dist/sweetalert2.all.min.js"></script>
@yield('scripts')

<style>
.followPicture {
    border-radius: 5px;
    width: 55px;
    height: 55px;
}

.be-comment-block {
    margin-bottom: 5px !important;
    border: 1px solid #edeff2;
    border-radius: 2px;
    border:1px solid #ffffff;
}


.be-img-comment {
    width: 25px;
    height: 25px;
    float: left;
    margin-bottom: 5px;
}

.be-ava-comment {
    width: 35px;
    height: 35px;
    border-radius: 50%;
}

.be-comment-content {
    margin-left: 50px;
}

.be-comment-content span {
    display: inline-block;
    width: 49%;
    margin-bottom: 5px;
}

.be-comment-name {
    font-size: 13px;
    font-family: 'Conv_helveticaneuecyr-bold';
}

.be-comment-content a {
    color: #383b43;
}

.be-comment-content span {
    display: inline-block;
    width: 49%;
    margin-bottom: 5px;
}

.be-comment-time {
    text-align: right;
}

.be-comment-time {
    font-size: 11px;
    color: #b4b7c1;
}

.be-comment-text {
    font-size: 13px;
    line-height: 18px;
    color: #7a8192;
    display: block;
    background: #f6f6f7;
    border: 1px solid #edeff2;
    padding: 10px 10px 10px 10px;
}

.addresslink{
    text-decoration: none;
    color:black;
}

.follow{
    float:right; 
    text-decoration: none;
}
.lcsBtn{
    border:none; 
    background-color: transparent;
    border-radius: 2px;
}

.divborder {
  width: 300px;
  padding: 5px;
  border-radius: 3px;
  width: 100%;
  margin-top: -5px;
}
.sharephoto {
  padding: 3px;
  border-radius: 3px;
}
.profileBtn{
    background-color: #555555;
    color: white;
    margin-left:5px;
}
.divbordershare {
  padding: 3px;
  width: 93%;
  border-radius: 3px;
  margin-top: -5px;
}

.sharephotoshare {
  padding: 3px;
  width: 93%;
  border-radius: 3px;
}

.postBtn {
  width: 100%;
  border-radius: 20px;
  display: block;
  resize: vertical;
}



/* Auth */

.full-page-wrapper {
    min-height: 90vh;
    padding-left: 0;
    padding-right: 0;
}

.auth {
    min-height: 90vh;
}

.auth.auth-bg-1 {
    background: url("../../images/auth/login_1.jpg");
    background-size: cover;
}

.auth.register-bg-1 {
    background: url("../../images/auth/register.jpg") center center no-repeat;
    background-size: cover;
}

.auth.theme-one .auto-form-wrapper {
    background: #fff;
    padding: 40px 40px 10px;
    border-radius: 4px;
    box-shadow: 0 -25px 37.7px 11.3px rgba(8, 143, 220, 0.07);
}

.auth.theme-one .auto-form-wrapper .form-group .input-group {
    height: 44px;
}

.auth.theme-one .auto-form-wrapper .form-group .input-group .form-control {
    border: 1px solid #cfd5db;
    border-right: none;
    border-radius: 6px 0 0 6px;
    height: 44px;
    line-height: 4px;
    display: inline-block;
    padding-top: 0px;
    padding-bottom: 0px;
}

.auth.theme-one .auto-form-wrapper .form-group .input-group .form-control:focus {
    border-right: none;
    border-color: #cfd5db;
}

.auth.theme-one .auto-form-wrapper .form-group .input-group .input-group-append {
    border-left: none;
}

.auth.theme-one .auto-form-wrapper .form-group .input-group .input-group-append .input-group-text {
    border-radius: 0 6px 6px 0;
    border-left: none;
    border-color: #cfd5db;
    color: #b6b6b6;
}

.auth.theme-one .auto-form-wrapper .form-group .submit-btn {
    font-family: "roboto", sans-serif;
    font-size: 13px;
    padding: 12px 8px;
    font-weight: 600;
}

.auth.theme-one .auto-form-wrapper .g-login {
    border: 1px solid #dee2e6;
    padding: 13px;
    font-size: 12px;
    font-weight: 600;
    background: transparent;
}

.auth.theme-one .auth-footer {
    list-style-type: none;
    padding-left: 0;
    margin-top: 20px;
    margin-bottom: 10px;
    display: flex;
    justify-content: center;
}

.auth.theme-one .auth-footer li {
    margin-right: 10px;
    line-height: 1;
    padding-right: 10px;
    border-right: 1px solid rgba(255, 255, 255, 0.4);
}

.auth.theme-one .auth-footer li:last-child {
    margin-right: 0;
    border-right: none;
}

.auth.theme-one .auth-footer li a {
    font-size: 13px;
    color: rgba(255, 255, 255, 0.4);
}

@media (max-width: 576px) {
    .auth.theme-one .auth-footer {
        justify-content: center;
    }
}

.auth.theme-one .footer-text { 
    color: rgba(255, 255, 255, 0.4);
}


.btn-block {
    display: block;
    width: 100%;
}

.btn-block+.btn-block {
    margin-top: 0.5rem;
}

input[type="submit"].btn-block,
input[type="reset"].btn-block,
input[type="button"].btn-block {
    width: 100%;
}

/* enable absolute positioning */
.inner-addon { 
    position: relative; 
}

/* style icon */
.inner-addon .bi {
  position: absolute;
  padding: 10px;
  pointer-events: none;
}

/* align icon */
.left-addon .bi  { left:  0px;}
.right-addon .bi { right: 0px;}

/* add padding  */
.left-addon input  { padding-left:  30px; }
.right-addon input { padding-right: 30px; }


/* new post button*/

/* Change styles for cancel button and delete button on extra small screens */
@media screen and (max-width: 400px) {
  .newpostBtn {
    width: 100%;
  }
}

/* Float cancel and delete buttons and add an equal width */
.newpostBtn{
  float: left;
  width: 100%;
  border-radius: 20px;
}


/* delete */
/* Change styles for cancel button and delete button on extra small screens */
@media screen and (max-width: 300px) {
  .cancelbtn, .deletebtn {
    width: 100%;
  }
}



/* Float cancel and delete buttons and add an equal width */
.cancelbtn, .deletebtn {
  float: left;
  width: 50%;
  border-radius: 0;
}
.clearfix{
    padding-top: 20px;
}

/* delete */

.editdelete{
    border:none; 
    background-color: transparent;
}

.like {
            color : blue;
        }
</style>
@include('sweetalert::alert')

</body>
</html>