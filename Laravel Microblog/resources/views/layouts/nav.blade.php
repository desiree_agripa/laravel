<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
               
                <!-- Search -->
                <div class="col-md-4  d-flex">
                <a class="navbar-brand" href="<?= url('/') ?>">
                    Microblog Laravel
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <form action="<?= url('/search', ); ?>" method="get" type="get">
                    <div class="d-flex">
                        <input type="hidden" name="post_id" value="">
                        <input type="text" class="form-control postBtn" name="query" size="100" placeholder="Search...">
                        <button class="btn btn-default createBtn" data-post_id=""  style="outline: none;" type="submit"> <i class="bi bi-search"></i></button>
                    </div>
                </form>
                
                </div>
                <!-- Search -->
                <!-- Logout -->
                    <div class="align-items-baseline d-flex">
                        <div>
                            @if ( Auth::user()->photo == '')
                            <img src="https://isobarscience.com/wp-content/uploads/2020/09/default-profile-picture1.jpg"  style="width: 35px; "alt="" class="rounded-circle btn-success">
                            @else
                            <img src="<?= asset('storage/'.Auth::user()->photo) ?>" alt=""  style="width: 35px; " class="rounded-circle btn-success">
                            @endif
                              <a  href="/profile/<?= Auth::user()->id ?>" style="text-decoration: none; color:black; padding-left: 10px">
                                    <?= Auth::user()->firstname ?> <?= Auth::user()->lastname ?>
                                </a>
                                <div class="dropdown" style="float: right">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle align-right" href="#"  role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                     <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                     <a class="dropdown-item" href="<?= route('profile.show', Auth::user()->id) ?>">
                                    <i class="bi bi-person-circle"></i> {{ __('My Profile') }}
                                    </a>
                                    <!-- -->
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="bi bi-box-arrow-right"></i> {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="<?= route('logout') ?>" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                     </div>
                                </div>
                        </div>   
                    </div>  
               <!-- Logout -->
            </div>
                
            </div>
        </nav>