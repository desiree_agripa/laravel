<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Mail\WelcomeMail;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\FollowsController;
use App\Http\Controllers\LikesController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\ShareController;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Middleware\XssSanitizer;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);
Route::post('like/{post}', [LikesController::class, 'store']);
Route::post('follow/{user}', [FollowsController::class, 'store']);

Route::post('/comment/store/', [CommentsController::class, 'store'])->name('comment.store');
Route::post('/comment/update/{comment}', [CommentsController::class, 'update']);
Route::delete('/comment/{comment}', [CommentsController::class, 'destroy']);
Route::get('/comment/{post}', [CommentsController::class, 'getComments']);

Route::get('/', [PostsController::class, 'index'])->middleware(['verified']); 
Route::post('/posts', [PostsController::class, 'store']);
Route::get('/posts/{post}', [PostsController::class, 'show'])->name('posts.show');
Route::delete('/posts/{post}', [PostsController::class, 'destroy']);
Route::post('/posts/update/{post}', [PostsController::class, 'update']);
Route::post('/posts/share', [PostsController::class, 'share'])->name('posts.share');

Route::get('/follow', [ProfileController::class, 'usersFollow'])->name('profiles.follow');
Route::get('/following/{user}', [ProfileController::class, 'showFollowing'])->name('profiles.following');
Route::get('/followers/{user}', [ProfileController::class, 'showFollower'])->name('profiles.followers');
Route::get('/profile/{user}/edit', [ProfileController::class, 'edit'])->name('profiles.edit');
Route::get('/profile/{user}', [ProfileController::class, 'index'])->name('profile.show');
Route::post('/profile/update/{user}', [ProfileController::class, 'update']);
Route::get('/search', [ProfileController::class, 'showSearch']);


